### Slack Token
To integrate with slack you'll need to generate a legacy token to supply to the app. You can find/generate the token here: https://api.slack.com/custom-integrations/legacy-tokens

### Usage
`SLACK_CLI_TOKEN=<token> npx pomocli [minutes (default: 25)]`
